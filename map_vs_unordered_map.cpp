#include <iostream>
#include <chrono>
#include <map>
#include <string>
#include <functional>
#include <unordered_map>
#include <random>
#include <algorithm>

int execution_stopwatch(std::function<void(void)> exec)
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	exec();

	end = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}

template<typename MapType>
class MapTester
{
public:
	MapTester(uint64_t setSize)
	{
		m_setSize = setSize;
		arraySet = new int[m_setSize];
	}
	~MapTester()
	{
		delete [] arraySet;
	}
	void fill();
	void erase();
	void find();
private:
		uint64_t m_setSize;
		int * arraySet = nullptr;
		MapType m_map;
};

template<typename MapType>
void MapTester<MapType>::fill()
{
	std::mt19937 gen(std::chrono::system_clock::now().time_since_epoch().count());
	
	for(auto i = 0; i < m_setSize; ++i)
	{
		arraySet[i] = i;
	}

	std::shuffle(arraySet, arraySet + m_setSize, gen);
	
	for(auto i = 0; i< m_setSize; ++i)
	{
		m_map.insert(std::make_pair(arraySet[i], std::to_string(arraySet[i])));
	}
}

template<typename MapType>
void MapTester<MapType>::erase()
{
	for(auto i = 0; i < m_setSize; ++i)
	{
		auto s = m_map.erase(arraySet[i]);
	}
}

template<typename MapType>
void MapTester<MapType>::find()
{
	for(auto i = 0; i < m_setSize; ++i)
	{
		auto it = m_map.find(arraySet[i]);
	}
}


int main()
{
	using Map = MapTester<std::map<int, std::string>>;
	using UnorderedMap = MapTester<std::unordered_map<int, std::string>>;
	for(auto i = 100; i <= 10'000'000; i*=10)
	{
		Map map(i);
		UnorderedMap unordered_map(i);
		std::cout<<"Test with "<<i<<" elements:"<<std::endl;
		
		std::cout<<"Filling std::map: "<<execution_stopwatch(std::bind(&Map::fill, &map))<<std::endl;
		std::cout<<"Filling std::unordered_map: "<<execution_stopwatch(std::bind(&UnorderedMap::fill, &unordered_map))<<std::endl;
		
		std::cout<<"Finding std::map: "<<execution_stopwatch(std::bind(&Map::find, &map))<<std::endl;
		std::cout<<"Finding std::unordered_map: "<<execution_stopwatch(std::bind(&UnorderedMap::find, &unordered_map))<<std::endl;
		
		std::cout<<"Erasing std::map: "<<execution_stopwatch(std::bind(&Map::erase, &map))<<std::endl;
		std::cout<<"Erasing std::unordered_map: "<<execution_stopwatch(std::bind(&UnorderedMap::erase, &unordered_map))<<std::endl;
		
	}
	
	return 0;
}
